from tkinter import *
import tkinter.messagebox as tmsg
import qrcode
from PIL import Image, ImageTk
from resizeimage import resizeimage
from tkinter.ttk import Combobox


def generate():
    global img
    i = id.get()
    n = name.get()
    m = department.get()
    d = designation.get()
    if i == '' or n == '' or m == '' or d == '':
        l['text'] = 'All Fields are required'
        
    else:
        qr_data = ('Id : ' + i + '\n' + 'Name : ' + n + '\n' + 'Department : ' + m + '\n' + 'Designation : ' + d)
        qr_code = qrcode.make(qr_data)
        qr_code = resizeimage.resize_cover(qr_code, [180, 180])
        qr_code.save(f'QR codes/{i}{n}.png')
        img = ImageTk.PhotoImage(file=f'QR codes/{i}{n}.png')
        l4.configure(image=img)
        l['text'] = 'QR Code Generated Successfully'


def clear():
    id.set('')
    name.set('')
    department.set('')
    designation.set('')
    l.configure(text=' ')
    l4.configure(text='NO QR\nAvailable')


def button_enter(event):
    n = event.widget.cget('text')
    if n == 'QR Generate':
        b1['bg'] = '#232130'
    elif n == 'Clear':
        b2['bg'] = '#232130'


def button_leave(event):
    n = event.widget.cget('text')
    if n == 'QR Generate':
        b1['bg'] = '#2575c4'
    elif n == 'Clear':
        b2['bg'] = 'grey'


root = Tk()
root.geometry('800x500')
root.resizable(0, 0)
root.title('QR code Generator')
root.wm_iconbitmap('icon.ico')
root.configure(bg='#576e80')

ss = "QR Code Generator"
l1 = Label(root, font=("Comicsansms", 20, "bold"), text='', bg='grey', fg='white', justify=CENTER, relief=GROOVE)
l1.pack(fill=X, side=TOP, ipady=5)
s = ''
count = 0


def slider():
    global s, count
    if count >= len(ss):
        count = -1
        s = ''
        l1['text'] = s
    else:
        s = s + ss[count]
        l1['text'] = s
    count += 1

    l1.after(200, slider)


slider()

l2 = LabelFrame(root, text='Employee Details', font=("Comicsansms", 20, "bold"), width=400, height=400, bg='#126658',
                fg='white')
l2.place(x=30, y=80)
Label(l2, text='ID', font=("Comicsansms", 12, "bold"), bg='#126658', fg='white').place(x=20, y=30)
Label(l2, text='Name', font=("Comicsansms", 12, "bold"), bg='#126658', fg='white').place(x=20, y=80)
Label(l2, text='Department', font=("Comicsansms", 12, "bold"), bg='#126658', fg='white').place(x=20, y=130)
Label(l2, text='Designation', font=("Comicsansms", 12, "bold"), bg='#126658', fg='white').place(x=20, y=180)

l = Label(l2, text='', font=("Comicsansms", 12, "bold"), bg='#126658', fg='white')
l.place(x=50, y=300)

id = StringVar()
e1 = Entry(l2, textvariable=id, font=("Comicsansms", 12, "bold"), relief=GROOVE, bg='lightgrey')
e1.place(x=200, y=30)
e1.focus()

name = StringVar()
e2 = Entry(l2, textvariable=name, font=("Comicsansms", 12, "bold"), relief=GROOVE,
           bg='lightgrey')
e2.place(x=200, y=80)

department = StringVar()
e2 = Entry(l2, textvariable=department, font=("Comicsansms", 12, "bold"), relief=GROOVE,
           bg='lightgrey')
e2.place(x=200, y=130)

designation = StringVar()
e2 = Entry(l2, textvariable=designation, font=("Comicsansms", 12, "bold"), relief=GROOVE,
           bg='lightgrey')
e2.place(x=200, y=180)

b1 = Button(l2, text='QR Generate', font=("Comicsansms", 12, "bold"), relief=GROOVE, fg='white', bg='#2575c4', width=10,
            command=generate, activebackground='grey', activeforeground='white')
b1.place(x=50, y=250)

b2 = Button(l2, text='Clear', font=("Comicsansms", 12, "bold"), relief=GROOVE, fg='white', bg='grey', width=10,
            command=clear, activebackground='grey', activeforeground='white')
b2.place(x=200, y=250)

l3 = LabelFrame(root, text='QR Code', font=("Comicsansms", 20, "bold"), width=250, height=400, bg='#126658', fg='white')
l3.place(x=500, y=80)

l4 = Label(l3, text='NO QR\nAvailable', font=("Comicsansms", 15, "bold"), bg='#3f51b5', fg='white', bd=1, relief=GROOVE)
l4.place(x=35, y=100, width=180, height=180)
b1.bind('<Enter>', button_enter)
b1.bind('<Leave>', button_leave)

b2.bind('<Enter>', button_enter)
b2.bind('<Leave>', button_leave)

root.mainloop()
