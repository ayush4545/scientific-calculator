from tkinter import *
import tkinter.messagebox as tmsg
import pyqrcode
from tkinter.ttk import Combobox


def generate():
    i = id.get()
    n = name.get()
    m = message.get()
    ex = text1.get()
    qr_message = 'Name : ' + n + '\n' + 'Id : ' + i + '\n' + 'Message : ' + m
    url = pyqrcode.create(qr_message)
    if ex == 'svg':
        url.svg('QR codes/{}{}.svg'.format(i, n), scale=8, module_color='#28224f')
    elif ex == 'png':
        url.svg('QR codes/{}{}.png'.format(i, n), scale=6, module_color='#28224f')

    tmsg.showinfo('Notification', 'Your QR Code is Generated\n of this name {}{}.{}'.format(i, n, ex))


def clear():
    id.set('')
    name.set('')
    message.set('')
    f1.current(0)


def button_enter(event):
    n = event.widget.cget('text')
    if n == 'Generate':
        b1['bg'] = '#232130'
    elif n == 'Clear':
        b2['bg'] = '#232130'


def button_leave(event):
    n = event.widget.cget('text')
    if n == 'Generate':
        b1['bg'] = 'grey'
    elif n == 'Clear':
        b2['bg'] = 'grey'


root = Tk()
root.geometry('600x400')
root.resizable(0, 0)
root.title('QR code Generator')
root.wm_iconbitmap('icon.ico')
root.configure(bg='#0d2945')

ss = "QR Code Generator Developed By Ayush Mishra"
l1 = Label(root, font=("Comicsansms", 20, "bold"), text='', bg='grey', fg='white', justify=CENTER, relief=GROOVE)
l1.pack(fill=X, side=TOP, ipady=5)
s = ''
count = 0


def slider():
    global s, count
    if count >= len(ss):
        count = -1
        s = ''
        l1['text'] = s
    else:
        s = s + ss[count]
        l1['text'] = s
    count += 1

    l1.after(200, slider)


slider()

Label(root, text='Enter Your ID', font=("Comicsansms", 15, "bold"), bg='#0d2945', fg='white').place(x=20, y=80)
Label(root, text='Enter Your Name', font=("Comicsansms", 15, "bold"), bg='#0d2945', fg='white').place(x=20, y=150)
Label(root, text='Enter Your Message', font=("Comicsansms", 15, "bold"), bg='#0d2945', fg='white').place(x=20, y=220)
Label(root, text='Select Extension', font=("Comicsansms", 15, "bold"), bg='#0d2945', fg='white').place(x=20, y=280)

id = StringVar()
e1 = Entry(root, textvariable=id, font=("Comicsansms", 12, "bold"), width=35, relief=GROOVE, fg='white', bg='lightgrey')
e1.place(x=250, y=80)
e1.focus()

name = StringVar()
e2 = Entry(root, textvariable=name, font=("Comicsansms", 12, "bold"), width=35, relief=GROOVE, fg='white',
           bg='lightgrey')
e2.place(x=250, y=150)

message = StringVar()
e2 = Entry(root, textvariable=message, font=("Comicsansms", 12, "bold"), width=35, relief=GROOVE, fg='white',
           bg='lightgrey')
e2.place(x=250, y=220)

b1 = Button(root, text='Generate', font=("Comicsansms", 12, "bold"), relief=GROOVE, fg='white', bg='grey', width=10,
            command=generate, activebackground='grey', activeforeground='white')
b1.place(x=290, y=340)

b2 = Button(root, text='Clear', font=("Comicsansms", 12, "bold"), relief=GROOVE, fg='white', bg='grey', width=10,
            command=clear, activebackground='grey', activeforeground='white')
b2.place(x=430, y=340)

text1 = StringVar()
extension = ['svg', 'png']
f1 = Combobox(root, textvariable=text1, state='readonly')
f1.config(width=40)
f1['values'] = [e for e in extension]
f1.current(0)
f1.place(x=250, y=280)

b1.bind('<Enter>', button_enter)
b1.bind('<Leave>', button_leave)

b2.bind('<Enter>', button_enter)
b2.bind('<Leave>', button_leave)

root.mainloop()
