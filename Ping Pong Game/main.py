import pygame
import sys

pygame.init()

# ====================variable============
screen_width = 820
screen_height = 500

window = pygame.display.set_mode((screen_width, screen_height))
pygame.display.set_caption('Ping Pong By Ayush Mishra')

icon = pygame.image.load('icon.ico')
pygame.display.set_icon(icon)
clock = pygame.time.Clock()

# ===================colors===============
white = (255, 255, 255)


def player(x, y, w, h, color):
    pygame.draw.rect(window, color, (x, y, w, h))
    pygame.display.update()


def ball(color, x, y):
    pygame.draw.circle(window, color, (x, y), 20)
    pygame.display.update()


def big_rect(x, y, w, h, color):
    pygame.draw.rect(window, color, (x, y, w, h))
    pygame.display.update()


def middle_net():
    for i in range(40, 470, 20):
        pygame.draw.circle(window, white, (400, i), 5)
    pygame.display.update()


def score(score1, score2):
    font = pygame.font.SysFont('comicsansms', 50)
    s1 = font.render(str(score1), True, white)
    s2 = font.render(str(score2), True, white)

    text1 = font.render('User', True, white)
    text2 = font.render('Player', True, white)
    text3 = font.render('Computer', True, white)
    text4 = font.render('Player', True, white)

    window.blit(s1, (50, 30))
    window.blit(s2, (750, 30))
    window.blit(text1, (150, screen_height / 2 - 100))
    window.blit(text2, (150, screen_height / 2 - 50))
    window.blit(text3, (450, screen_height / 2 - 100))
    window.blit(text4, (450, screen_height / 2 - 50))
    pygame.display.update()


def game_loop():
    exit_status = False

    x1 = 20
    y1 = 100
    x2 = 780
    y2 = 100
    x_ball = screen_width // 2
    y_ball = screen_height // 2
    ball_move_X = 5
    ball_move_Y = 5
    ball_radius = 20
    player1_y_change = 0
    player2_y_change = 0
    score1 = 0
    score2 = 0
    while not exit_status:
        window.fill((59, 63, 79))
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_UP:
                    player1_y_change = -5

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_DOWN:
                    player1_y_change = 5

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_UP:
                    player2_y_change = -5

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_DOWN:
                    player2_y_change = 5
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_UP or event.key == pygame.K_DOWN:
                    player1_y_change = 0

                if event.key == pygame.K_UP or event.key == pygame.K_DOWN:
                    player2_y_change = 0

        # ================player1 and player2 control=================================
        y1 += player1_y_change
        y2 += player2_y_change

        # =============================ball position change===========================
        x_ball += ball_move_X
        y_ball += ball_move_Y
        # =============================bliting the ball,paddle,score,middle net=======
        player(x1, y1, 20, 100, white)
        player(x2, y2, 20, 100, white)
        ball(white, x_ball, y_ball)
        big_rect(100, 0, 620, 20, white)
        big_rect(100, 480, 620, 20, white)
        middle_net()
        score(score1, score2)

        # ============================paddle boarder===================================
        if y1 <= 20:
            y1 = 20
        elif y1 > 380:
            y1 = 380
        if y2 <= 20:
            y2 = 20
        elif y2 > 380:
            y2 = 380

        # ===============================ball bounce and score check==========================
        if x_ball <= 0:
            ball_move_X = +5
            score2 += 1
        if x_ball >= screen_width:
            ball_move_X = -5
            score1 += 1
        if y_ball <= 20:
            ball_move_Y = +5
        if y_ball >= screen_height:
            ball_move_Y = -5

        # ==============computer movement============================
        if y2 < y_ball:
            player2_y_change = +5
        if y2 > y_ball:
            player2_y_change = -5

        # if x_ball<=20 and y1<350 and y_ball>400:
        #     score2+=1
        # =============paddle detection===============================
        if x_ball <= x1 + 20 and x_ball + ball_radius >= x1:
            if y_ball >= y1 and y_ball + ball_radius <= y1 + 100:
                ball_move_X = +5

        if x_ball + ball_radius >= x2 and x_ball <= x2 + 20:
            if y_ball >= y2 and y_ball + ball_radius <= y2 + 100:
                ball_move_X = -5

        pygame.display.update()
        clock.tick(60)


game_loop()
pygame.quit()
sys.exit()
