from tkinter import *
import math
import pyperclip as pc


def answer(event):
    global textvar
    print(event)
    text = event.widget.cget("text")
    print(text)
    if text == '=':
        if textvar.get().isdigit():
            value = int(textvar.get())
        else:
            try:
                value = eval(textvar.get())
            except Exception as e:
                print(e)
                value = 'error'

        textvar.set(value)
        text_field.update()
    elif text == 'C':
        textvar.set(" ")
        text_field.update()
    else:
        value = textvar.get() + text
        textvar.set(value)
        text_field.update()


def clear(event):
    text = textvar.get()
    text = text[0:len(text) - 1]
    textvar.set(text)


def copy():
    text = textvar.get()
    pc.copy(text)


def paste():
    textvar.set(pc.paste())


def scientific(event):
    global textvar
    text = event.widget.cget("text")
    value = " "
    print(text)
    if text == '√':
        value = str(math.sqrt(int(textvar.get())))
    elif text == '^':
        base, power = textvar.get().split(',')
        value = str(math.pow(int(base), int(power)))
    elif text == 'sinθ':
        value = str(math.sin(math.degrees(int(textvar.get()))))
    elif text == 'cosθ':
        value = str(math.cos(math.degrees(int(textvar.get()))))
    elif text == 'tanθ':
        value = str(math.tan(math.degrees(int(textvar.get()))))
    elif text == 'sinhθ':
        value = str(math.sinh(math.degrees(int(textvar.get()))))
    elif text == 'coshθ':
        value = str(math.cosh(math.degrees(int(textvar.get()))))
    elif text == 'tanhθ':
        value = str(math.tanh(math.degrees(int(textvar.get()))))
    elif text == 'n!':
        value = str(math.factorial(int(textvar.get())))
    elif text == 'log':
        value = str(math.log(int(textvar.get()), 2))
    elif text == 'x^2':
        value = str(int(textvar.get()) ** 2)
    elif text == 'x^3':
        value = str(int(textvar.get()) ** 3)
    elif text == 'e^x':
        value = str(math.exp(int(textvar.get())))
    elif text == '1/n':
        value = str(1 / textvar.get())
    elif text == '10^x':
        value = str(10 ** int(textvar.get()))
    elif text == 'Radian':
        value = str(math.radians(int(textvar.get())))
    elif text == 'π':
        value = str(math.pi)
    elif text == '∛':
        value = str(int(textvar.get()) ** (1 / 3))
    elif text == '∜':
        value = str(int(textvar.get()) ** (1 / 4))

    textvar.set(value)
    text_field.update()


standard = True


def convert():
    global standard
    if standard:
        buttonFrame.pack_forget()
        science_frame.pack(side=TOP, anchor='w', pady=10, padx=5)
        buttonFrame.pack(side=TOP, anchor='e', pady=10, padx=5)
        root.geometry('305x500')
        print('scienctific')
        standard = False
    else:
        print('Standard')
        science_frame.pack_forget()
        root.geometry('305x280')
        standard = True


root = Tk()
root.geometry('305x300')
root.title('All In One Calculator')
root.resizable(width=FALSE, height=False)
root.wm_iconbitmap('icon.ico')
root.configure(bg="#298577")

menu = Menu(root)
view = Menu(menu, tearoff=0, font='Lucid 10')
view.add_radiobutton(label="Standard", command=convert)
view.add_radiobutton(label="Scientific", command=convert)
view.add_separator()
view.add_command(label='Exit', command=root.destroy)
menu.add_cascade(label='View', menu=view)

edit = Menu(menu, tearoff=0, font='Lucid 10')
edit.add_command(label="Copy", command=copy)
edit.add_command(label="Paste", command=paste)
menu.add_cascade(label='Edit', menu=edit)

root.config(menu=menu)

screen_Frame = Frame(root, width=350, height=200, bg="#0d332d")
textvar = StringVar()

text_field = Entry(screen_Frame, width=350, font="lucida 20 bold", textvariable=textvar, relief=SUNKEN, justify=RIGHT)
text_field.pack(ipadx=15, pady=10, padx=5, fill=X)
screen_Frame.pack()

buttonFrame = Frame(root, bg="#293238")
buttonFrame.pack(side=TOP, anchor='w', pady=10, padx=5)

count = 1
for i in range(0, 3):
    for j in range(0, 3):
        btn = Button(buttonFrame, text=str(count), width=5, font='Verdana 12 bold', relief='ridge',
                     activebackground='#b4e084', padx=3, pady=3)
        btn.grid(row=i, column=j, pady=2, padx=2)
        btn.bind('<Button-1>', answer)
        count += 1

zerobtn = Button(buttonFrame, text='0', width=5, font='Verdana 12 bold', relief='ridge', activebackground='#b4e084',
                 padx=3, pady=3)
zerobtn.grid(row=3, column=0, pady=2, padx=2)
zerobtn.bind('<Button-1>', answer)

clearbtn = Button(buttonFrame, text='<-', width=12, font='Verdana 12 bold', relief='ridge',
                  activebackground='#b4e084', padx=3, pady=3)
clearbtn.grid(row=4, column=0, pady=2, columnspan=2)
clearbtn.bind('<Button-1>', clear)

dotbtn = Button(buttonFrame, text='.', width=5, font='Verdana 12 bold', relief='ridge', activebackground='#b4e084',
                padx=3, pady=3)
dotbtn.grid(row=3, column=1, pady=2, padx=2)
dotbtn.bind('<Button-1>', answer)

divbtn = Button(buttonFrame, text='/', width=5, font='Verdana 12 bold', relief='ridge', activebackground='#b4e084',
                padx=3, pady=3)
divbtn.grid(row=0, column=3, pady=2, padx=2)
divbtn.bind('<Button-1>', answer)

mulbtn = Button(buttonFrame, text='*', width=5, font='Verdana 12 bold', relief='ridge', activebackground='#b4e084',
                padx=3, pady=3)
mulbtn.grid(row=1, column=3, pady=2, padx=2)
mulbtn.bind('<Button-1>', answer)

subbtn = Button(buttonFrame, text='-', width=5, font='Verdana 12 bold', relief='ridge', activebackground='#b4e084',
                padx=3, pady=3)
subbtn.grid(row=2, column=3, pady=2, padx=2)
subbtn.bind('<Button-1>', answer)

addbtn = Button(buttonFrame, text='+', width=5, font='Verdana 12 bold', relief='ridge', activebackground='#b4e084',
                padx=3, pady=3)
addbtn.grid(row=3, column=3, pady=2, padx=2)
addbtn.bind('<Button-1>', answer)

equalbtn = Button(buttonFrame, text='=', width=5, font='Verdana 12 bold', relief='ridge', activebackground='#b4e084',
                  padx=3, pady=3)
equalbtn.grid(row=3, column=2, pady=2, padx=2)
equalbtn.bind('<Button-1>', answer)

allclearbtn = Button(buttonFrame, text='C', width=12, font='Verdana 12 bold', relief='ridge',
                     activebackground='#b4e084', padx=3, pady=3)
allclearbtn.grid(row=4, column=2, pady=2, columnspan=2, padx=2)
allclearbtn.bind('<Button-1>', answer)

####################scientific calculator###################

science_frame = Frame(root, bg="#293238")

srootbtn = Button(science_frame, text='√', width=5, font='Verdana 12 bold', relief='ridge',
                  activebackground='#b4e084', padx=3, pady=3)
srootbtn.grid(row=0, column=0, pady=2, padx=3)
srootbtn.bind('<Button-1>', scientific)

srootbtn = Button(science_frame, text='^', width=5, font='Verdana 12 bold', relief='ridge',
                  activebackground='#b4e084', padx=3, pady=3)
srootbtn.grid(row=0, column=1, pady=2, padx=2)
srootbtn.bind('<Button-1>', scientific)

trootbtn = Button(science_frame, text='∛', width=5, font='Verdana 12 bold', relief='ridge',
                  activebackground='#b4e084', padx=3, pady=3)
trootbtn.grid(row=0, column=2, pady=2, padx=2)
trootbtn.bind('<Button-1>', scientific)

frootbtn = Button(science_frame, text='∜', width=5, font='Verdana 12 bold', relief='ridge',
                  activebackground='#b4e084', padx=3, pady=3)
frootbtn.grid(row=0, column=3, pady=2, padx=2)
frootbtn.bind('<Button-1>', scientific)

sinbtn = Button(science_frame, text='sinθ', width=5, font='Verdana 12 bold', relief='ridge',
                activebackground='#b4e084', padx=3, pady=3)
sinbtn.grid(row=1, column=0, pady=2, padx=2)
sinbtn.bind('<Button-1>', scientific)

cosbtn = Button(science_frame, text='cosθ', width=5, font='Verdana 12 bold', relief='ridge',
                activebackground='#b4e084', padx=3, pady=3)
cosbtn.grid(row=1, column=1, pady=2, padx=2)
cosbtn.bind('<Button-1>', scientific)

tanbtn = Button(science_frame, text='tanθ', width=5, font='Verdana 12 bold', relief='ridge',
                activebackground='#b4e084', padx=3, pady=3)
tanbtn.grid(row=1, column=2, pady=2, padx=2)
tanbtn.bind('<Button-1>', scientific)

sinhbtn = Button(science_frame, text='sinhθ', width=5, font='Verdana 12 bold', relief='ridge',
                 activebackground='#b4e084', padx=3, pady=3)
sinhbtn.grid(row=2, column=0, pady=2, padx=2)
sinhbtn.bind('<Button-1>', scientific)

coshbtn = Button(science_frame, text='coshθ', width=5, font='Verdana 12 bold', relief='ridge',
                 activebackground='#b4e084', padx=3, pady=3)
coshbtn.grid(row=2, column=1, pady=2, padx=2)
coshbtn.bind('<Button-1>', scientific)

tanhbtn = Button(science_frame, text='tanhθ', width=5, font='Verdana 12 bold', relief='ridge',
                 activebackground='#b4e084', padx=3, pady=3)
tanhbtn.grid(row=2, column=2, pady=2, padx=2)
tanhbtn.bind('<Button-1>', scientific)

Factbtn = Button(science_frame, text='n!', width=5, font='Verdana 12 bold', relief='ridge',
                 activebackground='#b4e084', padx=3, pady=3)
Factbtn.grid(row=1, column=3, pady=2, padx=2)
Factbtn.bind('<Button-1>', scientific)

modbtn = Button(science_frame, text='%', width=5, font='Verdana 12 bold', relief='ridge', activebackground='#b4e084',
                padx=3, pady=3)
modbtn.grid(row=2, column=3, pady=2, padx=2)
modbtn.bind('<Button-1>', answer)

spowbtn = Button(science_frame, text='x^2', width=5, font='Verdana 12 bold', relief='ridge',
                 activebackground='#b4e084', padx=3, pady=3)
spowbtn.grid(row=3, column=0, pady=2, padx=2)
spowbtn.bind('<Button-1>', scientific)

tpowbtn = Button(science_frame, text='x^3', width=5, font='Verdana 12 bold', relief='ridge',
                 activebackground='#b4e084', padx=3, pady=3)
tpowbtn.grid(row=3, column=1, pady=2, padx=2)
tpowbtn.bind('<Button-1>', scientific)

invbtn = Button(science_frame, text='1/n', width=5, font='Verdana 12 bold', relief='ridge',
                activebackground='#b4e084', padx=3, pady=3)
invbtn.grid(row=3, column=2, pady=2, padx=2)
invbtn.bind('<Button-1>', scientific)

tenpowbtn = Button(science_frame, text='10^x', width=5, font='Verdana 12 bold', relief='ridge',
                   activebackground='#b4e084', padx=3, pady=3)
tenpowbtn.grid(row=3, column=3, pady=2, padx=2)
tenpowbtn.bind('<Button-1>', scientific)

logbtn = Button(science_frame, text='log', width=5, font='Verdana 12 bold', relief='ridge',
                activebackground='#b4e084', padx=3, pady=3)
logbtn.grid(row=4, column=0, pady=2, padx=2)
logbtn.bind('<Button-1>', scientific)

piebtn = Button(science_frame, text='π', width=5, font='Verdana 12 bold', relief='ridge', activebackground='#b4e084',
                padx=3, pady=3)
piebtn.grid(row=4, column=1, pady=2, padx=2)
piebtn.bind('<Button-1>', scientific)

expbtn = Button(science_frame, text='e^x', width=5, font='Verdana 12 bold', relief='ridge',
                activebackground='#b4e084', padx=3, pady=3)
expbtn.grid(row=4, column=2, pady=2, padx=2)
expbtn.bind('<Button-1>', scientific)

radianbtn = Button(science_frame, text='Radian', width=5, font='Verdana 12 bold', relief='ridge',
                   activebackground='#b4e084', padx=3, pady=3)
radianbtn.grid(row=4, column=3, pady=2, padx=2)
radianbtn.bind('<Button-1>', scientific)

root.mainloop()
