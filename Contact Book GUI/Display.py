from tkinter import *
import datetime
from ttkthemes import themed_tk as tk
from tkinter import ttk
import tkinter.messagebox as tmsg
from database import insert, search
import sqlite3

conn = sqlite3.connect('contact.db')
cur = conn.cursor()


def displaycontact(img, id):
    row = search(id)
    top_level = Toplevel()
    top_level.geometry('650x550+350+200')
    top_level.resizable(False, False)
    top_level.title('Display Person')
    top_level.wm_iconbitmap('icon.ico')
    top_frame = Frame(top_level, height=150, bg='white')
    top_frame.pack(fill=X)

    bottom_frame = Frame(top_level, height=500, bg='#f2880f')
    bottom_frame.pack(fill=X)

    name = str(row[1])
    name = name.capitalize()
    # pic1 = PhotoImage(file='book.png')
    Label(top_frame, text="  " + name, image=img, compound=LEFT, font="Helvetica 20 bold", fg='#08c29d',
          bg='white').place(x=150, y=20)

    # showing Date in the window
    date = "Date: " + str(datetime.datetime.now().date())
    Label(top_frame, text=date, font="Helvetica 10 bold", fg='#f2880f', bg='white').place(x=500, y=8)

    Label(bottom_frame, text='Showing Details', font="Helvetica 15 bold", bg='#f2880f', fg='white').place(x=250, y=5)

    Label(bottom_frame, text='FirstName', font="Helvetica 12 bold", bg='#f2880f', fg='white').place(x=100, y=70)
    Label(bottom_frame, text='LastName', font="Helvetica 12 bold", bg='#f2880f', fg='white').place(x=100, y=110)
    Label(bottom_frame, text='Email ID', font="Helvetica 12 bold", bg='#f2880f', fg='white').place(x=100, y=150)
    Label(bottom_frame, text='Mobile NO.', font="Helvetica 12 bold", bg='#f2880f', fg='white').place(x=100, y=190)
    Label(bottom_frame, text='Address', font="Helvetica 12 bold", bg='#f2880f', fg='white').place(x=100, y=230)

    E1 = Label(bottom_frame, text=row[1], font="Helvetica 12 bold", bg='#f2880f', fg='white')
    E1.place(x=220, y=70)

    E2 = Label(bottom_frame, text=row[2], font="Helvetica 12 bold", bg='#f2880f', fg='white')
    E2.place(x=220, y=110)

    E3 = Label(bottom_frame, text=row[3], font="Helvetica 12 bold", bg='#f2880f', fg='white')
    E3.place(x=220, y=150)

    E4 = Label(bottom_frame, text=row[4], font="Helvetica 12 bold", bg='#f2880f', fg='white')
    E4.place(x=220, y=190)

    E5 = Label(bottom_frame, text=row[5], font="Helvetica 12 bold", bg='#f2880f', fg='white')
    E5.place(x=220, y=230)
