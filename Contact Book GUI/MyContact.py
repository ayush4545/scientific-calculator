from tkinter import *
import datetime
from ttkthemes import themed_tk as tk
from tkinter import ttk
from NewContact import newcontact
from update import updatecontact
from database import insert, view, delete
from Display import displaycontact


def add(img, tl):
    new = newcontact(img)
    tl.destroy()


def update(img, list1, tp):
    selected_item = list1.curselection()
    item = list1.get(selected_item)
    item = item.split(" ")
    id = int(item[0])
    up = updatecontact(img, id)
    tp.destroy()


def show(img, list1):
    selected_item = list1.curselection()
    item = list1.get(selected_item)
    item = item.split(" ")
    id = int(item[0])
    disp = displaycontact(img, id)


def delete_contact(list1):
    selected_item = list1.curselection()
    item = list1.get(selected_item)
    item = item.split(" ")
    id = int(item[0])
    delete(id)
    list1.delete(selected_item)


def MyContact(img):
    top_level = Toplevel()
    top_level.geometry('650x550+350+200')
    top_level.resizable(False, False)
    top_level.title('My Contacts')

    top_level.wm_iconbitmap('icon.ico')
    top_frame = Frame(top_level, height=150, bg='white')
    top_frame.pack(fill=X)
    bottom_frame = Frame(top_level, height=500, bg='#8a7b5a')
    bottom_frame.pack(fill=X)

    # pic1 = PhotoImage(file='book.png')
    Label(top_frame, text='  My Contacts', image=img, compound=LEFT, font="Helvetica 20 bold", fg='#179299',
          bg='white').place(x=150, y=20)

    # showing Date in the window
    date = "Date: " + str(datetime.datetime.now().date())
    Label(top_frame, text=date, font="Helvetica 10 bold", fg='#8a7b5a', bg='white').place(x=500, y=8)

    scrollbar = Scrollbar(bottom_frame, orient=VERTICAL)
    scrollbar.pack(side=RIGHT, fill=Y)

    list1 = Listbox(bottom_frame, yscrollcommand=scrollbar.set, width=50, height=27, bg='#d3f0e7')
    list1.pack(fill=Y, side=TOP, anchor='nw')
    scrollbar.config(command=list1.yview)

    peoples = view()
    count = 2
    list1.insert(0, "S.no  First Name  Last Name  Mobile Number")
    list1.insert(1, "____|__________|_________|________________________________________")
    for people in peoples:
        list1.insert(count,
                     str(people[0]) + "         " + people[1] + "             " + people[2] + "         " + people[4])
        count += 1

    b1 = ttk.Button(bottom_frame, text='Add', command=lambda: add(img, top_level))
    b1.place(x=450, y=50)

    b2 = ttk.Button(bottom_frame, text='Update', command=lambda: update(img, list1, top_level))
    b2.place(x=450, y=100)

    b3 = ttk.Button(bottom_frame, text='Display', command=lambda: show(img, list1))
    b3.place(x=450, y=150)

    b3 = ttk.Button(bottom_frame, text='Delete', command=lambda: delete_contact(list1))
    b3.place(x=450, y=200)
