from tkinter import *
import datetime
from ttkthemes import themed_tk as tk
from tkinter import ttk
import tkinter.messagebox as tmsg
from database import insert, search
import sqlite3

conn = sqlite3.connect('contact.db')
cur = conn.cursor()


def update(firstname, lastname, email, mobile, address, id, tp):
    fname = str(firstname.get())
    lname = str(lastname.get())
    mail = str(email.get())
    phone = str(mobile.get())
    add = address.get("1.0", "end-1c")
    add = str(add)

    if fname and lname and mail and phone and add != "":
        try:
            query = "update 'contact' set first_name=?,last_name=?,email_id=?,mobile=?,address=? where id=?"
            cur.execute(query, (fname, lname, mail, phone, add, id))
            conn.commit()
            tmsg.showinfo('Success', 'Contact Updated')
            tp.destroy()
        except Exception as e:
            tmsg.showerror('Error', str(e), icon='warning')

    else:
        tmsg.showerror('Error', 'Please fill Required Fields', icon='warning')


def updatecontact(img, id):
    row = search(id)
    top_level = Toplevel()
    top_level.geometry('650x550+350+200')
    top_level.resizable(False, False)
    top_level.title('Update Details')
    top_level.wm_iconbitmap('icon.ico')
    top_frame = Frame(top_level, height=150, bg='white')
    top_frame.pack(fill=X)

    bottom_frame = Frame(top_level, height=500, bg='#48404f')
    bottom_frame.pack(fill=X)

    # pic1 = PhotoImage(file='book.png')
    Label(top_frame, text='  Update Contact', image=img, compound=LEFT, font="Helvetica 20 bold", fg='#128c5d',
          bg='white').place(x=150, y=20)

    # showing Date in the window
    date = "Date: " + str(datetime.datetime.now().date())
    Label(top_frame, text=date, font="Helvetica 10 bold", fg='#48404f', bg='white').place(x=500, y=8)

    Label(bottom_frame, text=' Update Details ', font="Helvetica 15 bold", bg='#48404f', fg='white').place(x=250, y=5)

    Label(bottom_frame, text='FirstName', font="Helvetica 12 bold", bg='#48404f', fg='white').place(x=100, y=70)
    Label(bottom_frame, text='LastName', font="Helvetica 12 bold", bg='#48404f', fg='white').place(x=100, y=110)
    Label(bottom_frame, text='Email ID', font="Helvetica 12 bold", bg='#48404f', fg='white').place(x=100, y=150)
    Label(bottom_frame, text='Mobile NO.', font="Helvetica 12 bold", bg='#48404f', fg='white').place(x=100, y=190)
    Label(bottom_frame, text='Address', font="Helvetica 12 bold", bg='#48404f', fg='white').place(x=100, y=230)

    firstname = StringVar()
    firstname.set(row[1])
    E1 = ttk.Entry(bottom_frame, textvariable=firstname, width=55)
    E1.place(x=220, y=70)
    E1.focus()

    lastname = StringVar()
    lastname.set(row[2])
    E2 = ttk.Entry(bottom_frame, textvariable=lastname, width=55)

    E2.place(x=220, y=110)

    email = StringVar()
    email.set(row[3])
    E3 = ttk.Entry(bottom_frame, textvariable=email, width=55)

    E3.place(x=220, y=150)

    mobile = StringVar()
    mobile.set(row[4])
    E4 = ttk.Entry(bottom_frame, textvariable=mobile, width=55)

    E4.place(x=220, y=190)

    address = Text(bottom_frame, width=42, height=4)
    address.insert(END, row[5])
    address.place(x=220, y=230)

    ttk.Button(bottom_frame, text='Update', width=20,
               command=lambda: update(firstname, lastname, email, mobile, address, id, top_level)).place(x=250, y=330)
