import sqlite3



def insert(fname,lname,mail,phone,add):
    conn = sqlite3.connect('contact.db')
    cur = conn.cursor()
    query = "insert into 'contact' (first_name,last_name,email_id,mobile,Address) values(?,?,?,?,?)"
    cur.execute(query, (fname, lname, mail, phone, add))
    conn.commit()
    conn.close()

def view():
    conn = sqlite3.connect('contact.db')
    cur = conn.cursor()
    cur.execute("SELECT * FROM contact")
    rows = cur.fetchall()
    conn.commit()
    conn.close()
    return rows

def delete(id):
    conn = sqlite3.connect('contact.db')
    cur = conn.cursor()
    query="DELETE FROM 'contact' WHERE id=? "
    cur.execute(query, (id,))
    conn.commit()
    conn.close()

def search(id):
    conn = sqlite3.connect('contact.db')
    cur = conn.cursor()
    query="Select * from 'contact' where id=?"
    cur.execute(query,(id,))
    rows = cur.fetchone()
    conn.commit()
    conn.close()
    return rows


