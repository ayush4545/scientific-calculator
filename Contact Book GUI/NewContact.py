from tkinter import *
import datetime
from ttkthemes import themed_tk as tk
from tkinter import ttk
import tkinter.messagebox as tmsg
from database import insert
import sqlite3

conn = sqlite3.connect('contact.db')
cur = conn.cursor()


def submit(firstname, lastname, email, mobile, address):
    fname = str(firstname.get())
    lname = str(lastname.get())
    mail = str(email.get())
    phone = str(mobile.get())
    add = address.get("1.0", "end-1c")
    add = str(add)

    if fname and lname and mail and phone and add != "":
        try:
            insert(fname, lname, mail, phone, add)
            tmsg.showinfo('Success', 'Contact Added')
        except Exception as e:
            tmsg.showerror('Error', str(e), icon='warning')

    else:
        tmsg.showerror('Error', 'Please fill Required Fields', icon='warning')


def newcontact(img):
    top_level = Toplevel()
    top_level.geometry('650x550+350+200')
    top_level.resizable(False, False)
    top_level.title('New Contact')
    top_level.wm_iconbitmap('icon.ico')
    top_frame = Frame(top_level, height=150, bg='white')
    top_frame.pack(fill=X)

    bottom_frame = Frame(top_level, height=500, bg='#752f16')
    bottom_frame.pack(fill=X)

    # pic1 = PhotoImage(file='book.png')
    Label(top_frame, text='  My Contact Book', image=img, compound=LEFT, font="Helvetica 20 bold", fg='#128c5d',
          bg='white').place(x=150, y=20)

    # showing Date in the window
    date = "Date: " + str(datetime.datetime.now().date())
    Label(top_frame, text=date, font="Helvetica 10 bold", fg='#8c5b12', bg='white').place(x=500, y=8)

    Label(bottom_frame, text='Add New Contact', font="Helvetica 15 bold", bg='#752f16', fg='white').place(x=250, y=5)

    Label(bottom_frame, text='FirstName', font="Helvetica 12 bold", bg='#752f16', fg='white').place(x=100, y=70)
    Label(bottom_frame, text='LastName', font="Helvetica 12 bold", bg='#752f16', fg='white').place(x=100, y=110)
    Label(bottom_frame, text='Email ID', font="Helvetica 12 bold", bg='#752f16', fg='white').place(x=100, y=150)
    Label(bottom_frame, text='Mobile NO.', font="Helvetica 12 bold", bg='#752f16', fg='white').place(x=100, y=190)
    Label(bottom_frame, text='Address', font="Helvetica 12 bold", bg='#752f16', fg='white').place(x=100, y=230)

    firstname = StringVar()
    E1 = ttk.Entry(bottom_frame, textvariable=firstname, width=55)
    E1.place(x=220, y=70)
    E1.focus()

    lastname = StringVar()
    E2 = ttk.Entry(bottom_frame, textvariable=lastname, width=55)

    E2.place(x=220, y=110)

    email = StringVar()
    E3 = ttk.Entry(bottom_frame, textvariable=email, width=55)

    E3.place(x=220, y=150)

    mobile = StringVar()
    E4 = ttk.Entry(bottom_frame, textvariable=mobile, width=55)

    E4.place(x=220, y=190)

    address = StringVar()
    address = Text(bottom_frame, width=42, height=4)
    address.place(x=220, y=230)

    ttk.Button(bottom_frame, text='Submit', width=20,
               command=lambda: submit(firstname, lastname, email, mobile, address)).place(x=250, y=330)
