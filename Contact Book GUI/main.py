from tkinter import *
import datetime
from ttkthemes import themed_tk as tk
from tkinter import ttk
import tkinter.messagebox as tmsg
from MyContact import MyContact
from NewContact import newcontact


def mycontact(pic):
    people = MyContact(pic)


def about():
    tmsg.showinfo('About Us', 'This is Contact Book that save people\n important contacts securely')


def new_contact(pic):
    new = newcontact(pic)


def main():
    root = tk.ThemedTk()
    root.get_themes()
    root.set_theme('radiance')
    root.geometry('650x550+350+200')
    root.resizable(False, False)
    root.title('My Contact Book')
    root.wm_iconbitmap('icon.ico')

    # divide whole window into 2 frame

    top_frame = Frame(root, height=150, bg='white')
    top_frame.pack(fill=X)

    bottom_frame = Frame(root, height=500, bg='#264059')
    bottom_frame.pack(fill=X)

    pic = PhotoImage(file='book.png')
    pic1 = PhotoImage(file='people.png')
    pic2 = PhotoImage(file='people.png')

    Label(top_frame, text='   My Contact Book', image=pic, compound=LEFT, font="Helvetica 20 bold", fg='#295c8c',
          bg='white').place(x=150, y=20)

    # showing Date in the window
    date = "Date: " + str(datetime.datetime.now().date())
    Label(top_frame, text=date, font="Helvetica 10 bold", fg='#167567', bg='white').place(x=500, y=8)

    # adding button

    b1 = ttk.Button(bottom_frame, text='New Contact', width=15, command=lambda: new_contact(pic2))
    b1.place(x=250, y=50)

    b2 = ttk.Button(bottom_frame, text='My Contacts', width=15, command=lambda: mycontact(pic1))
    b2.place(x=250, y=120)

    b3 = ttk.Button(bottom_frame, text='About us', width=15, command=about)
    b3.place(x=250, y=190)

    root.mainloop()


if __name__ == '__main__':
    main()
