from tkinter import *
from tkinter.ttk import Progressbar
from pytube import *
from PIL import Image, ImageTk
import requests
import io
import os
import tkinter.messagebox as tmsg


def search():
    try:
        url = url_text.get()

        a = YouTube(url)
        # ======Fetch the size as per type
        if val.get() == 'Video':
            selected_file = a.streams.filter(progressive=True).first()

        if val.get() == 'Audio':
            selected_file = a.streams.filter(only_audio=True).first()
        # ============Convert the size in MB
        global size_inbytes
        size_inbytes = selected_file.filesize
        max_size = size_inbytes / 1024000
        mb = round(max_size)
        size['text'] = 'Total Size:' + str(mb) + ' MB'

        # ==========updating the frame===========
        title = a.title
        thumbnail = a.thumbnail_url
        response = requests.get(thumbnail)
        img_byte = io.BytesIO(response.content)
        img = Image.open(img_byte)
        img = img.resize((214, 214), Image.ANTIALIAS)
        pic['file'] = img
        desc = a.description[:200]
        lf['text'] = title
        video_text.insert(END, desc)
        downloadbtn['state'] = NORMAL
    except Exception as e:
        tmsg.showerror('Error Status', 'Video URL is Required', icon='warning')


def progress(stream, chunks, bytes_remaining):
    global size_inbytes
    percantage = (float(abs(bytes_remaining - size_inbytes) / size_inbytes)) * float(100)
    downloadingbar['value'] = percantage
    download.config(text=f'Download : {str(round(percantage, 2))}%')
    if round(percantage, 2) == 100:
        l3.config(text='Download Completed', fg='green')
        downloadbtn['state'] = DISABLED


def download_():
    url = url_text.get()

    a = YouTube(url, on_progress_callback=progress)
    # ======Fetch the size as per type
    if val.get() == 'Video':
        selected_file = a.streams.filter(progressive=True).first()
        selected_file.download('Videos/')
    if val.get() == 'Audio':
        selected_file = a.streams.filter(only_audio=True).first()
        selected_file.download('Audios/')


def clear():
    val.set('Video')
    url_text.set('')
    downloadbtn['state'] = DISABLED
    downloadingbar['value'] = 0
    download.config(text='Download : 0%')
    size.config(text='Total Size : MB')
    video_image.config(image='')
    lf.config(text='Video Title')
    video_text.delete('1.0', END)
    l3['text'] = " "


root = Tk()
root.geometry('700x500')
root.title('Youtube Downloader')
root.resizable(0, 0)
root.wm_iconbitmap('icon.ico')

l = Label(root, text='Youtube Downloader By Ayush Mishra', font='poppin 15 bold', bg='grey', justify=CENTER, fg='white')
l.pack(side=TOP, fill=X)

l1 = Label(root, text='Video URL', font='verdana 15 bold')
l1.place(x=50, y=45)

url_text = StringVar()
E1 = Entry(root, textvariable=url_text, width=70, relief=GROOVE)
E1.focus()
E1.place(x=180, y=52)

lf = LabelFrame(root, text='Video Title', width=550, height=250, bg='grey', fg='white', font='verdana 10 bold')
lf.place(x=5, y=90)

pic = PhotoImage()
video_image = Label(lf, text='Video \n Image', font=('times new roman', 10, 'bold'), bg='lightgrey'
                    )
video_image.place(x=5, y=5, width=214, height=214)

video_desc = LabelFrame(lf, text='Description', font=('times new roman', 13, 'bold'), bg='white', width=315, height=215)
video_desc.place(x=225, y=5)

video_text = Text(video_desc, bg='lightgrey', width=38, height=12, relief=GROOVE)
video_text.place(x=1, y=0)

file_type = Label(root, text='File Type', font='verdana 15')
file_type.place(x=570, y=100)

val = StringVar()
val.set('Video')
videobtn = Radiobutton(root, text='Video', val='Video', variable=val, font=('times new roman', 15, 'bold'))
videobtn.place(x=570, y=150)

audiobtn = Radiobutton(root, text='Audio', val='Audio', variable=val, font=('times new roman', 15, 'bold'))
audiobtn.place(x=570, y=210)

searchbtn = Button(root, text='Search', font=('times new roman', 15, 'bold'), relief=GROOVE, width=8, command=search)
searchbtn.place(x=570, y=280)

size = Label(root, text='Total Size: MB', font=('poppins', 13, 'bold'))
size.place(x=5, y=350)

download = Label(root, text='Download : 0%', font=('poppins', 14, 'bold'))
download.place(x=210, y=350)

clearbtn = Button(root, text='Clear', font=('poppins', 13, 'bold'), relief=GROOVE, width=10, command=clear)
clearbtn.place(x=420, y=350)

downloadbtn = Button(root, text='Download', font=('poppins', 13, 'bold'), relief=GROOVE, width=10, command=download_,
                     state=DISABLED)
downloadbtn.place(x=570, y=350)

downloadingbar = Progressbar(root, orient=HORIZONTAL, length=680, mode='determinate')
downloadingbar.place(x=10, y=400)

l3 = Label(root, text='', font=('poppins', 13, 'bold'))
l3.place(x=300, y=450)

# =================making Directory========================
if os.path.exists('Videos') == False:
    os.mkdir('Videos')
if os.path.exists('Audios') == False:
    os.mkdir('Audios')
root.mainloop()
