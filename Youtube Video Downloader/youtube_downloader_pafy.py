from tkinter import *
from tkinter.ttk import Progressbar
import pafy
import youtube_dl
from PIL import Image, ImageTk
import io
import threading
from tkinter import filedialog
import requests

download_index = 0

total1 = 0


def search():
    getdetail = threading.Thread(target=getvideo)
    getdetail.start()


def put_img(video):
    global photo
    thumbnail = video.thumb
    response = requests.get(thumbnail)
    img_byte = io.BytesIO(response.content)
    img = Image.open(img_byte)
    img = img.resize((214, 214), Image.ANTIALIAS)
    photo = ImageTk.PhotoImage(img)
    video_image.configure(image=photo)


def getvideo():
    global streams
    file_text.delete(0, END)
    url = url_text.get()
    video = pafy.new(url)

    streams = video.allstreams
    count = 0

    title = video.title
    desc = f'''
          Author : {video.author}
          ID : {video.videoid}
          Duration : {video.duration}
          Rating : {video.rating}
          Views : {video.viewcount}
          '''
    lf['text'] = title[:30]
    text.insert(END, desc)
    put_img(video)
    downloadbtn['state'] = NORMAL
    for i in streams:
        size = '{:0.1f}'.format(i.get_filesize() // (1024 * 1024))
        data = str(count) + '.'.ljust(3, ' ') + str(i.quality).ljust(10, ' ') + str(i.extension).ljust(5, ' ') + str(
            i.mediatype) + ' ' + str(size).rjust(10, ' ') + 'MB'
        file_text.insert(END, data)
        count += 1


def selectcursor(event):
    global download_index
    d = file_text.get(file_text.curselection())
    d = d[:3]
    download_index = int(''.join(x for x in d if x.isdigit()))
    print(d)
    print(download_index, type(download_index))


def download_():
    getdata = threading.Thread(target=DownloadVideoData)
    getdata.start()


def DownloadVideoData():
    global download_index, streams, max_size
    file = filedialog.askdirectory()
    print(file)
    l3['text'] = 'Downloading.......'

    def update(total, recvd, ratio, rate, eta):
        global total1, max_size
        remainin = total - recvd
        total1 = float('{:0.1f}'.format(total // (1024 * 1024)))
        downloadingbar.config(maximum=total1)
        t = '{:0.1f} MB'.format(total // (1024 * 1024))
        recieved = '{:0.5} MB'.format(recvd // (1024 * 1024))
        total_size.configure(text='Total Size: ' + str(total1) + 'MB')
        recieved_size.configure(text='Recieved Size: ' + recieved)
        percantage = (float(abs(remainin - total) / total)) * float(100)
        download.config(text=f'Download : {str(round(percantage, 2))}%')
        downloadingbar['value'] = recvd / (1024 * 1024)

    print(download_index, type(download_index))
    print(streams)
    streams[download_index].download(filepath=file, quiet=True, callback=update)
    l3['text'] = 'Downloading Complete'


def clear():
    file_text.delete(0, END)
    url_text.set('')
    downloadbtn['state'] = DISABLED
    downloadingbar['value'] = 0
    download.config(text='Download : 0%')
    total_size.config(text='Total Size : MB')
    recieved_size.config(text='Recieved Size: MB')
    video_image.config(image='')
    lf.config(text='Video Title')
    text.delete('1.0', END)
    l3['text'] = " "


root = Tk()
root.geometry('910x500')
root.title('Youtube Downloader')
root.resizable(0, 0)
root.wm_iconbitmap('icon.ico')
root.configure(bg='#31394a')
ss = 'Youtube Downloader By Ayush Mishra'
l = Label(root, text='', font='poppin 15 bold', bg='#202226', justify=CENTER, fg='white')
l.pack(side=TOP, fill=X)

s = ''
count = 0


def slider():
    global s, count
    if count >= len(ss):
        count = -1
        s = ''
        l['text'] = s
    else:
        s = s + ss[count]
        l['text'] = s
    count += 1

    l.after(200, slider)


slider()

l1 = Label(root, text='Video URL', font='verdana 15 bold', bg='#31394a', fg='white')
l1.place(x=80, y=45)

url_text = StringVar()
E1 = Entry(root, textvariable=url_text, width=80, relief=GROOVE)
E1.focus()
E1.place(x=230, y=52)

lf = LabelFrame(root, text='Video Title', width=550, height=250, bg='grey', fg='white', font='verdana 10 bold')
lf.place(x=5, y=90)

video_image = Label(lf, text='Video \n Image', font=('times new roman', 10, 'bold'), bg='lightgrey'
                    )
video_image.place(x=5, y=5, width=214, height=214)

description = LabelFrame(lf, text='Description', width=310, height=214, font='verdana 13 bold')
description.place(x=225, y=5)
text = Text(description, font='verdana 12 bold', width=27, height=10, bg='#2d2d2e', fg='white')
text.place(x=2, y=2)

file_type = LabelFrame(root, text='File Type', font='poppins 12 bold', relief=GROOVE, width=320, height=260, bg='grey',
                       fg='white')
file_type.place(x=570, y=89)

scrollbar = Scrollbar(file_type, orient=VERTICAL)
scrollbar.grid(row=0, column=1, rowspan=5, sticky='ns')

file_text = Listbox(file_type, width=52, height=14, selectmode=SINGLE,
                    yscrollcommand=scrollbar.set, selectbackground='sky blue', bg='#2d2d2e', fg='white')
scrollbar.config(command=file_text.yview)
file_text.grid(row=0, column=0)

file_text.bind('<<ListboxSelect>>', selectcursor)

searchbtn = Button(root, text='Search', font=('poppins', 13, 'bold'), relief=GROOVE, width=8, command=search)
searchbtn.place(x=750, y=45)

total_size = Label(root, text='Total Size: MB', font=('poppins', 12, 'bold'), bg='#31394a', fg='white')
total_size.place(x=5, y=350)

recieved_size = Label(root, text='Recieved Size: MB', font=('poppins', 12, 'bold'), bg='#31394a', fg='white')
recieved_size.place(x=185, y=350)

download = Label(root, text='Download : 0%', font=('poppins', 12, 'bold'), bg='#31394a', fg='white')
download.place(x=400, y=350)

clearbtn = Button(root, text='Clear', font=('poppins', 13, 'bold'), relief=GROOVE, width=10, command=clear)
clearbtn.place(x=600, y=350)

downloadbtn = Button(root, text='Download', font=('poppins', 13, 'bold'), relief=GROOVE, width=10,
                     state=DISABLED, command=download_)
downloadbtn.place(x=750, y=350)

# =================================================Progressbar=====================================================
downloadingbar = Progressbar(root, orient=HORIZONTAL, length=880, mode='determinate', maximum=total1)
downloadingbar.place(x=10, y=400)

l3 = Label(root, text='', font=('poppins', 15, 'bold'), fg='white', bg='#31394a')
l3.place(x=400, y=450)

root.mainloop()
