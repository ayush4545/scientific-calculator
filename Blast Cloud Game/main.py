import pygame
import random
import sys
import time
from pygame import mixer

pygame.init()


# ==========================Functions===================================
def new_word():
    global word_x, word_y, text, word_choice, press_keyword, speed_y
    word_x = random.randint(200, 700)
    word_y = 0
    word_choice = random.choice(word_list)
    press_keyword = ''
    speed_y += 0.02
    text = font.render(word_choice, True, (71, 53, 52))


###########variables========================

screen_width = 900
screen_height = 600
exit_status = False
word_list = ['apple', 'mango', 'banana', 'orange', 'cloud', 'blast', 'game', 'man', 'eat', 'drink', 'go', 'keyboard',
             'music', 'apology', 'time', 'date']
font = pygame.font.SysFont('ComicSansms', 45)
speed_y = 0.3
point = 0
text1 = font.render('Hit the Space To Restart the Game......', True, (255, 0, 0))
# ===============screen==================================
win = pygame.display.set_mode((screen_width, screen_height))
pygame.display.set_caption('Blast Cloud Word Game')

# ===================================music================
pygame.mixer.music.load('sound/bgmuisc.wav')
pygame.mixer.music.set_volume(0.2)
channel_0 = pygame.mixer.Channel(0)
channel_1 = pygame.mixer.Channel(1)
pygame.mixer.music.play()
blast_music = pygame.mixer.Sound('sound/boom.wav')
channel_0.set_volume(0.5)
loss_music = pygame.mixer.Sound('sound/loss1.wav')
channel_1.set_volume(0.3)
# ======================images================
icon = pygame.image.load('image/jj.png')
pygame.display.set_icon(icon)

bg1 = pygame.image.load('image/1.png')
bg1 = pygame.transform.scale(bg1, (screen_width, screen_height)).convert_alpha()

bg2 = pygame.image.load('image/2.png')
bg2 = pygame.transform.scale(bg2, (screen_width, screen_height)).convert_alpha()

cloud = pygame.image.load('image/cloud.png')
blast = pygame.image.load('image/explosion1.gif')
pygame.transform.scale(blast, (200, 150)).convert_alpha()
new_word()

while not exit_status:
    if point > 100:
        win.blit(bg2, (0, 0))
    else:
        win.blit(bg1, (0, 0))
    win.blit(cloud, (int(word_x - 40), int(word_y - 20)))
    win.blit(text, (int(word_x), int(word_y)))
    word_y += speed_y
    word_caption = font.render(word_choice, True, (71, 53, 52))
    win.blit(word_caption, (10, 50))
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            exit_status = True
            sys.exit()

        elif (event.type == pygame.KEYDOWN):
            press_keyword += pygame.key.name(event.key)

            if word_choice.startswith(press_keyword):
                text = font.render(press_keyword, True, (43, 59, 94))

                if word_choice == press_keyword:
                    point += len(word_choice)
                    channel_0.play(blast_music, maxtime=600)
                    channel_0.set_volume(0.5)
                    win.blit(blast, (int(word_x - 40), int(word_y - 20)))

                    pygame.display.update()
                    time.sleep(0.1)
                    new_word()
            else:
                text = font.render(press_keyword, True, (255, 0, 0))
                press_keyword = ''
    score_caption = font.render(str(point), True, (71, 53, 52))
    win.blit(score_caption, (10, 5))
    if (word_y > screen_height - 10):
        bb = pygame.mixer.Channel(1).get_busy()
        if bb == 1:
            pass
        else:
            channel_1.play(loss_music, maxtime=6000)
        win.blit(text1, (70, 250))
        pygame.display.update()
        event = pygame.event.wait()
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()

        if (event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE):
            point = 0
            speed_y = 0.3
            new_word()

    pygame.display.update()
