from tkinter import *
from PIL import Image, ImageEnhance, ImageFilter, ImageDraw
from tkinter import filedialog
import os

global clip1


def load():
    global file, filename, clip1
    file = filedialog.askopenfilename()
    filename = os.path.basename(file)
    clip1 = Image.open(file)
    load_label['text'] = filename + ' ' + str(clip1.size)


def convert():
    global file, clip1, convert_img_show

    n = name.get()
    sub = subject.get()
    p = f"{n}.{sub}"
    clip1.save(p)


def show_1(event):
    global clip1
    clip1.show()


def resize():
    global clip1, clip_resize, filename
    clip1
    x1 = x.get()
    y1 = y.get()
    clip_resize = clip1.resize((round(x1), round(y1)))
    clip_resize.save(filename)


def show_2(event):
    global clip_resize
    clip_resize.show()


def color():
    global clip1, clip_color
    val = colorscale.get()
    n = color_name.get()
    enhance = ImageEnhance.Color(clip1)
    clip_color = enhance.enhance(val)
    clip_color.save(n + '.jpg')


def show_3(event):
    global clip_color
    clip_color.show()


def brightness():
    global clip1, clip_brightness
    val = briscale.get()
    n = Briname.get()
    enhance = ImageEnhance.Brightness(clip1)
    clip_brightness = enhance.enhance(val)
    clip_brightness.save(n + '.jpg')


def show_4(event):
    global clip_brightness
    clip_brightness.show()


def contrast():
    global clip1, clip_contrast
    val = contrasescale.get()
    n = conname.get()
    enhance = ImageEnhance.Brightness(clip1)
    clip_contrast = enhance.enhance(val)
    clip_contrast.save(n + '.jpg')


def show_5(event):
    global clip_contrast
    clip_contrast.show()


def blur():
    global clip1, clip_blur
    val = blurscale.get()
    n = blurname.get()
    enhance = clip1.filter(ImageFilter.GaussianBlur(radius=val))
    clip_blur.save(n + '.jpg')


def show_6(event):
    global clip_blur
    clip_blur.show()


def load_merge():
    global file2, filename2, clip2
    file2 = filedialog.askopenfilename()
    filename2 = os.path.basename(file2)
    clip2 = Image.open(file2)


def merge():
    global clip1, clip_merge, clip2
    n = mergename.get()
    val = clip2.size
    clip1 = clip1.resize(val)
    image1_size = clip1.size

    clip_merge = Image.new('RGB', (2 * image1_size[0], image1_size[1]), (250, 250, 250))
    clip_merge.paste(clip1, (0, 0))
    clip_merge.paste(clip2, (image1_size[0], 0))
    clip_merge.save(n + '.jpg')


def show_7(event):
    global clip_merge
    clip_merge.show()


global clip_crop


def crop():
    global clip1, clip_crop
    left = leftname.get()
    upper = uppername.get()
    right = rightname.get()
    lower = lowername.get()
    n = cropname.get()

    clip_crop = clip1.crop((left, upper, right, lower))
    clip_crop.save(n + '.jpg')


def show_8(event):
    global clip_crop
    clip_crop.show()


def horizontal_left_right_flip():
    global clip1, clip_hleft_right_flip, filename
    clip_hleft_flip = clip1.transpose(Image.FLIP_LEFT_RIGHT)
    clip_hleft_flip.save(filename)
    clip_hleft_flip.show()


def vertical_left_right_flip():
    global clip1, clip_vleft_right_flip, filename
    clip_vleft_flip = clip1.transpose(Image.FLIP_TOP_BOTTOM)
    clip_vleft_flip.save(filename)
    clip_vleft_flip.show()


def r_90():
    global clip1, clip_r90, filename
    clip_r90 = clip1.transpose(Image.ROTATE_90)
    clip_r90.save(filename)
    clip_r90.show()


def r_180():
    global clip1, clip_r180, filename
    clip_r180 = clip1.transpose(Image.ROTATE_180)
    clip_r180.save(filename)
    clip_r180.show()


def r_270():
    global clip1, clip_r270, filename
    clip_r270 = clip1.transpose(Image.ROTATE_270)
    clip_r270.save(filename)
    clip_r270.show()


def write_text():
    global clip1, clip_text, img, filename
    clip_text = clip1
    img = ImageDraw.Draw(clip_text)

    img.text((x1.get(), y1.get()), textname.get(), fill=(R.get(), G.get(), B.get()))
    clip_text.save(filename)


def show_9(event):
    global clip_text
    clip_text.show()


root = Tk()
root.geometry('1060x650')
root.title('Migma Image Editor')
root.resizable(0, 0)
root.wm_iconbitmap('icon.ico')
root.configure(bg='#46444f')
loadbtn = Button(root, text='Load Image', font=("poppin", 20, "bold"), relief=GROOVE, justify=CENTER, bg='lightgrey',
                 command=load)
loadbtn.pack(pady=15)

load_label = Label(root, text='', font=("poppin", 15, "bold"), relief=GROOVE, justify=CENTER, bg='grey', fg='white')
load_label.pack(fill=X)

# -----------------------------------------------convert & Resize-------------------------------------------------------
convert_resize = LabelFrame(root, text='Convert to Different extension & Resize', width=530, height=180,
                            font=("poppin", 15, "bold"),bg='#696563',fg='white')
convert_resize.place(x=0, y=120)
extension = ['jpg', 'png', 'jpeg', 'pdf', 'gif']

subject = StringVar()
subject.set('Select Extension')
f2 = OptionMenu(convert_resize, subject, *extension, )
f2.config(bg='lightgrey', relief=GROOVE, bd=2)
f2.place(x=50, y=20)

name = StringVar()
name.set('New Name of Image')
E1 = Entry(convert_resize, textvariable=name, relief=GROOVE, bd=3)
E1.place(x=200, y=25)

conbtn = Button(convert_resize, text='Convert', font=("poppin", 10, "bold"), relief=GROOVE, justify=CENTER,
                bg='lightgrey', command=convert)
conbtn.place(x=350, y=20)

show1 = Button(convert_resize, text='Display', font=("poppin", 10, "bold"), relief=GROOVE, justify=CENTER,
               bg='lightgrey')
show1.place(x=450, y=20)

l1 = Label(convert_resize, text='Dimension', font=("poppin", 12, "bold"))
l1.place(x=50, y=80)

x = IntVar()
e2 = Entry(convert_resize, textvariable=x, relief=GROOVE, width=5, bd=3)
e2.place(x=200, y=80)

l2 = Label(convert_resize, text='X', font=("poppin", 12, "bold"))
l2.place(x=250, y=80)

y = IntVar()
e3 = Entry(convert_resize, textvariable=y, relief=GROOVE, width=5, bd=3)
e3.place(x=290, y=80)

resizebtn = Button(convert_resize, text='Resize', font=("poppin", 10, "bold"), relief=GROOVE, justify=CENTER,
                   bg='lightgrey', command=resize)
resizebtn.place(x=350, y=80)

show2 = Button(convert_resize, text='Display', font=("poppin", 10, "bold"), relief=GROOVE, justify=CENTER,
               bg='lightgrey')
show2.place(x=450, y=80)

# -----------------------------------------------Color & Brightness-------------------------------------------------------
cbcb = LabelFrame(root, text='Color & Brightness', width=530, height=180, font=("poppin", 15, "bold"),bg='#696563',fg='white')
cbcb.place(x=530, y=120)

colorscale = Scale(cbcb, from_=0, to=5, orient=HORIZONTAL, sliderlength=5, cursor='circle', troughcolor='grey')
colorscale.place(x=50, y=0)

color_name = StringVar()
color_name.set('New Name of Image')
E4 = Entry(cbcb, textvariable=color_name, relief=GROOVE, bd=3)
E4.place(x=200, y=20)

colorbtn = Button(cbcb, text='Color', font=("poppin", 10, "bold"), relief=GROOVE, justify=CENTER, bg='lightgrey',
                  command=color)
colorbtn.place(x=350, y=20)

show3 = Button(cbcb, text='Display', font=("poppin", 10, "bold"), relief=GROOVE, justify=CENTER, bg='lightgrey')
show3.place(x=450, y=20)

briscale = Scale(cbcb, from_=0, to=5, orient=HORIZONTAL, sliderlength=5, cursor='circle', troughcolor='grey')
briscale.place(x=50, y=60)

Briname = StringVar()
Briname.set('New Name of Image')
E5 = Entry(cbcb, textvariable=Briname, relief=GROOVE, bd=3)
E5.place(x=200, y=80)

brightbtn = Button(cbcb, text='Brigtness', font=("poppin", 10, "bold"), relief=GROOVE, justify=CENTER, bg='lightgrey',
                   command=brightness)
brightbtn.place(x=350, y=80)

show4 = Button(cbcb, text='Display', font=("poppin", 10, "bold"), relief=GROOVE, justify=CENTER, bg='lightgrey')
show4.place(x=450, y=80)

# ----------------------------------------------- Contrast & Blur-------------------------------------------------------
cb = LabelFrame(root, text='Contrast & Blur', width=530, height=180, font=("poppin", 15, "bold"),bg='#696563',fg='white')
cb.place(x=530, y=300)
contrasescale = Scale(cb, from_=0, to=5, orient=HORIZONTAL, sliderlength=5, cursor='circle', troughcolor='grey')
contrasescale.place(x=50, y=0)

conname = StringVar()
conname.set('New Name of Image')
E6 = Entry(cb, textvariable=conname, relief=GROOVE, bd=3)
E6.place(x=200, y=20)

contbtn = Button(cb, text='Contrase', font=("poppin", 10, "bold"), relief=GROOVE, justify=CENTER, bg='lightgrey',
                 command=contrast)
contbtn.place(x=350, y=20)

show5 = Button(cb, text='Display', font=("poppin", 10, "bold"), relief=GROOVE, justify=CENTER, bg='lightgrey')
show5.place(x=450, y=20)

blurscale = Scale(cb, from_=1, to=10, orient=HORIZONTAL, sliderlength=5, cursor='circle', troughcolor='grey')
blurscale.place(x=50, y=60)

blurname = StringVar()
blurname.set('New Name of Image')
E6 = Entry(cb, textvariable=blurname, relief=GROOVE, bd=3)
E6.place(x=200, y=80)

blurbtn = Button(cb, text='Blur', font=("poppin", 10, "bold"), relief=GROOVE, justify=CENTER, bg='lightgrey')
blurbtn.place(x=350, y=80)

show6 = Button(cb, text='Display', font=("poppin", 10, "bold"), relief=GROOVE, justify=CENTER, bg='lightgrey')
show6.place(x=450, y=80)

# -----------------------------------------------Merge & crop------------------------------------------------
mc = LabelFrame(root, text='Merging Images & Crop Image', width=530, height=180, font=("poppin", 15, "bold"),bg='#696563',fg='white')
mc.place(x=0, y=300)
merge = Button(mc, text='Choose Other Image', font=("poppin", 8, "bold"), relief=GROOVE, justify=CENTER,
               bg='lightgrey', command=load_merge)
merge.place(x=50, y=20)

mergename = StringVar()
mergename.set('New Name of Image')
E7 = Entry(mc, textvariable=mergename, relief=GROOVE, bd=3)
E7.place(x=200, y=20)

mergebtn = Button(mc, text='Merge', font=("poppin", 10, "bold"), relief=GROOVE, justify=CENTER, bg='lightgrey',
                  command=merge)
mergebtn.place(x=350, y=20)

show7 = Button(mc, text='Display', font=("poppin", 10, "bold"), relief=GROOVE, justify=CENTER, bg='lightgrey')
show7.place(x=450, y=20)

leftname = IntVar()
E8 = Entry(mc, textvariable=leftname, relief=GROOVE, width=4, bd=3)
E8.place(x=50, y=80)

uppername = IntVar()
E9 = Entry(mc, textvariable=uppername, relief=GROOVE, width=4, bd=3)
E9.place(x=85, y=80)

rightname = IntVar()
E10 = Entry(mc, textvariable=rightname, relief=GROOVE, width=4, bd=3)
E10.place(x=120, y=80)

lowername = IntVar()
E11 = Entry(mc, textvariable=lowername, relief=GROOVE, width=4, bd=3)
E11.place(x=155, y=80)

cropname = StringVar()
cropname.set('New Name of Image')
E12 = Entry(mc, textvariable=cropname, relief=GROOVE, bd=3)
E12.place(x=200, y=80)

cropbtn = Button(mc, text='Crop', font=("poppin", 10, "bold"), relief=GROOVE, justify=CENTER, bg='lightgrey',
                 command=crop)
cropbtn.place(x=350, y=80)

show8 = Button(mc, text='Display', font=("poppin", 10, "bold"), relief=GROOVE, justify=CENTER, bg='lightgrey')
show8.place(x=450, y=80)

# ---------------------------Flip and Rotate---------------------------------------------------------------------------
rf = LabelFrame(root, text='Flip Image & Rotate Image', width=530, height=180, font=("poppin", 15, "bold"),bg='#696563',fg='white')
rf.place(x=0, y=480)

# ---------------------------Horizontal Flip---------------------------------------------------------------------------
hflipbtn = Button(rf, text='Horizontal Flip', font=("poppin", 10, "bold"), relief=GROOVE, justify=CENTER,
                  bg='lightgrey', command=horizontal_left_right_flip)
hflipbtn.place(x=100, y=20)

degree_sign = u"\N{DEGREE SIGN}"

# ---------------------------Vertical Flip---------------------------------------------------------------------------
vflipbtn = Button(rf, text='Vertical Flip Left', font=("poppin", 10, "bold"), relief=GROOVE, justify=CENTER,
                  bg='lightgrey', command=vertical_left_right_flip)
vflipbtn.place(x=270, y=20)

# ---------------------------Rotate--------------------------------------------------------------------------------

rotate_90 = Button(rf, text='Rotate 90' + degree_sign, font=("poppin", 10, "bold"), relief=GROOVE, justify=CENTER,
                   bg='lightgrey', command=r_90)
rotate_90.place(x=80, y=80)

rotate_180 = Button(rf, text='Rotate 180' + degree_sign, font=("poppin", 10, "bold"), relief=GROOVE, justify=CENTER,
                    bg='lightgrey', command=r_180)
rotate_180.place(x=200, y=80)

rotate_270 = Button(rf, text='Rotate 270' + degree_sign, font=("poppin", 10, "bold"), relief=GROOVE, justify=CENTER,
                    bg='lightgrey', command=r_270)
rotate_270.place(x=320, y=80)

# ----------------------------------Write Text on Image-----------------------------------------
wt = LabelFrame(root, text='Write Text on Image', width=530, height=180, font=("poppin", 15, "bold"),bg='#696563',fg='white')
wt.place(x=530, y=480)

Label(wt, text='Coordinates', font=("poppin", 12, "bold")).place(x=50, y=20)
x1 = IntVar()
e13 = Entry(wt, textvariable=x1, relief=GROOVE, width=5, bd=3)
e13.place(x=200, y=20)

l2 = Label(wt, text='X', font=("poppin", 12, "bold"))
l2.place(x=250, y=20)

y1 = IntVar()
e14 = Entry(wt, textvariable=y1, relief=GROOVE, width=5, bd=3)
e14.place(x=290, y=20)

Label(wt, text='RGB Color', font=("poppin", 12, "bold"), wraplength=80).place(x=350, y=10)

R = IntVar()
e15 = Entry(wt, textvariable=R, relief=GROOVE, width=4, bd=3)
e15.place(x=410, y=20)

G = IntVar()
e16 = Entry(wt, textvariable=G, relief=GROOVE, width=4, bd=3)
e16.place(x=450, y=20)

B = IntVar()
e17 = Entry(wt, textvariable=B, relief=GROOVE, width=4, bd=3)
e17.place(x=490, y=20)

Label(wt, text='Text', font=("poppin", 12, "bold")).place(x=50, y=80)

textname = StringVar()
textname.set('Enter Text')
E18 = Entry(wt, textvariable=textname, relief=GROOVE, bd=3)
E18.place(x=200, y=85)

writebtn = Button(wt, text='Write', font=("poppin", 10, "bold"), relief=GROOVE, justify=CENTER, bg='lightgrey',
                  command=write_text)
writebtn.place(x=350, y=80)

show9 = Button(wt, text='Display', font=("poppin", 10, "bold"), relief=GROOVE, justify=CENTER, bg='lightgrey')
show9.place(x=450, y=80)

show1.bind('<Button-1>', show_1)
show2.bind('<Button-1>', show_2)
show3.bind('<Button-1>', show_3)
show4.bind('<Button-1>', show_4)
show5.bind('<Button-1>', show_5)
show6.bind('<Button-1>', show_6)
show7.bind('<Button-1>', show_7)
show8.bind('<Button-1>', show_8)
show9.bind('<Button-1>', show_9)

root.mainloop()
