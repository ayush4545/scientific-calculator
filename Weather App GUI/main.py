import requests
from tkinter import *
from ttkthemes import themed_tk as tk
from tkinter import ttk
import tkinter.messagebox as tmsg


def get_weather(city):
    api = 'http://api.openweathermap.org/data/2.5/weather?q=' + city + '&appid=e3a89339f361cb0fdd3fba1259bdd653'
    try:
        json_data = requests.get(api).json()
        condition = json_data['weather'][0]['main']
        temp = int(json_data['main']['temp'] - 273.15)
        min_temp = int(json_data['main']['temp_min'] - 273.15)
        max_temp = int(json_data['main']['temp_max'] - 273.15)
        pressure = json_data['main']['pressure']
        humidity = json_data['main']['humidity']
        wind = json_data['wind']['speed']
        # sunrise=time.strftime("%I:%M:%S",time.gmtime(json_data['sys']['sunrise']-18000))
        # sunset=time.strftime("%I:%M:%S",time.gmtime(json_data['sys']['sunset']-18000))
        img = json_data['weather'][0]['icon']
        values = {
            'condition': condition,
            'temperature': temp,
            'max_temp': max_temp,
            'min_temp': min_temp,
            'pressure': pressure,
            'humidity': humidity,
            'wind': wind,
            'photo': img
        }
        return values
    except Exception as e:
        tmsg.showerror('Error', 'Unavailabe to connect \n Please Check Internet Connection')


def search():
    city = var.get()
    weather = get_weather(city)
    print(weather)
    degree_sign = u"\N{DEGREE SIGN}"
    Location['text'] = city.capitalize()
    temp['text'] = str(weather['temperature']) + degree_sign + 'C'
    condition['text'] = weather['condition']
    photo['file'] = 'img/' + weather['photo'] + '.png'
    other_details['text'] = 'maximum Temperature: ' + str(
        weather['max_temp']) + degree_sign + 'C' + '\n' + 'Minimum Temperature: ' + str(
        weather['min_temp']) + degree_sign + 'C' + '\n' + 'Pressure: ' + str(
        weather['pressure']) + '\n' + 'Wind: ' + str(weather['wind'])


root = tk.ThemedTk()
root.get_themes()
root.set_theme('radiance')
root.geometry('600x650')
root.wm_maxsize(width=600, height=500)
root.wm_minsize(width=600, height=500)
root.title('Weather App')
root.wm_iconbitmap('icon.ico')

ttk.Label(root, text='Enter the city name', font="poppins 15 bold").pack(pady=10)

var = StringVar()
E1 = ttk.Entry(root, textvariable=var, font='poppins 10 bold', width=60)
E1.focus()
E1.place(x=100, y=50)

b = ttk.Button(root, text='Search', command=search)
b.place(x=230, y=80)

Location = ttk.Label(root, text='', font='poppins 35 bold')
Location.place(x=200, y=130)

photo = PhotoImage()

temp = ttk.Label(root, text=" ", font='poppins 35 bold')
temp.place(x=245, y=200)
condition = ttk.Label(root, text=" ", font='poppins 15 bold')
condition.place(x=250, y=260)
image = Label(root, image=photo)
image.place(x=250, y=300)

other_details = ttk.Label(root, text=" ", font='poppins 10 bold')
other_details.place(x=200, y=400)

root.mainloop()
