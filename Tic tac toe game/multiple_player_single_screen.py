import pygame
import sys
import numpy as np

from pygame import mixer
import random

pygame.init()
# ===========================Variables=======================================
screen_width = 600
screen_height = 600
line_width = 15
board_rows = 3
board_cols = 3
CIRCLE_RADIUS = 60
CIRCLE_WIDTH = 15
CROSS_WIDTH = 25
SQUARE_SIZE = 200
SPACE = 55
# ======================Board============
board = np.zeros((board_rows, board_cols))
# ===========================Colors==========================================
bg_color = (42, 46, 94)
welcome_bg_Color=(15, 112, 90)
white=(255,255,255)
line_color = (20, 166, 149)
circle_color = (127, 161, 157)
CROSS_COLOR = (155, 179, 171)

window = pygame.display.set_mode((screen_width, screen_height))
pygame.display.set_caption('Tic tac toe By Ayush Mishra')

icon = pygame.image.load('icon.ico')
pygame.display.set_icon(icon)
clock = pygame.time.Clock()


def draw_line():
    # horizontal line 1
    pygame.draw.line(window, line_color, (0, 200), (600, 200), line_width)
    # horizontal line 2
    pygame.draw.line(window, line_color, (0, 400), (600, 400), line_width)
    # vertical line 1
    pygame.draw.line(window, line_color, (200, 0), (200, 600), line_width)
    # vertical line 1
    pygame.draw.line(window, line_color, (400, 0), (400, 600), line_width)


def mark_square(row, col, player):
    board[row][col] = player


def available_square(row, col):
    if board[row][col] == 0:
        return True
    else:
        return False


def is_board_full():
    for row in range(board_rows):
        for col in range(board_cols):
            if board[row][col] == 0:
                return False
    return True


def draw_figure():
    for row in range(board_rows):
        for col in range(board_cols):
            if board[row][col] == 2:

                pygame.draw.circle(window, circle_color, (int(col * 200 + 100), int(row * 200 + 100)), CIRCLE_RADIUS,
                                   CIRCLE_WIDTH)
            elif board[row][col] == 1:
                pygame.draw.line(window, CROSS_COLOR,
                                 (col * SQUARE_SIZE + SPACE, row * SQUARE_SIZE + SQUARE_SIZE - SPACE),
                                 (col * SQUARE_SIZE + SQUARE_SIZE - SPACE, row * SQUARE_SIZE + SPACE), CROSS_WIDTH)
                pygame.draw.line(window, CROSS_COLOR, (col * SQUARE_SIZE + SPACE, row * SQUARE_SIZE + SPACE),
                                 (col * SQUARE_SIZE + SQUARE_SIZE - SPACE, row * SQUARE_SIZE + SQUARE_SIZE - SPACE),
                                 CROSS_WIDTH)


def check_win(player):
    # vertical win check
    for col in range(board_cols):
        if board[0][col] == player and board[1][col] == player and board[2][col] == player:
            draw_vertical_winning_line(col, player)
            return True

    # horizontal win check
    for row in range(board_rows):
        if board[row][0] == player and board[row][1] == player and board[row][2] == player:
            draw_horizontal_winning_line(row, player)
            return True

    # asc diagonal win check
    if board[2][0] == player and board[1][1] == player and board[0][2] == player:
        draw_asc_diagonal(player)
        return True

    # desc diagonal win chek
    if board[0][0] == player and board[1][1] == player and board[2][2] == player:
        draw_desc_diagonal(player)
        return True

    return False


def draw_vertical_winning_line(col, player):
    posX = col * SQUARE_SIZE + SQUARE_SIZE // 2

    if player == 1:
        color = CROSS_COLOR
    elif player == 2:
        color = circle_color

    pygame.draw.line(window, color, (posX, 15), (posX, screen_height - 15), 15)


def draw_horizontal_winning_line(row, player):
    posY = row * SQUARE_SIZE + SQUARE_SIZE // 2

    if player == 1:
        color = CROSS_COLOR
    elif player == 2:
        color = circle_color

    pygame.draw.line(window, color, (15, posY), (screen_width - 15, 15), 15)


def draw_asc_diagonal(player):
    if player == 1:
        color = CROSS_COLOR
    elif player == 2:
        color = circle_color

    pygame.draw.line(window, color, (15, screen_height - 15), (screen_width - 15, 15), 15)


def draw_desc_diagonal(player):
    if player == 1:
        color = CROSS_COLOR
    elif player == 2:
        color = circle_color

    pygame.draw.line(window, color, (15, 15), (screen_width - 15, screen_height - 15), 15)


def restart():
    window.fill(bg_color)
    draw_line()
    for row in range(board_rows):
        for col in range(board_cols):
            board[row][col] = 0

def last_screen(player):
    exit_status = True

    while exit_status:
        window.fill(welcome_bg_Color)
        last_screen_text(player)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_r:
                    restart()
                    game_loop()

        pygame.display.update()
        clock.tick(60)
def game_loop():
    global player
    exit_status = True
    game_over = False
    player = 1
    window.fill(bg_color)
    draw_line()
    while exit_status:

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()

            if event.type == pygame.MOUSEBUTTONDOWN and not game_over:
                mouse_x = event.pos[0]
                mouse_y = event.pos[1]

                click_row = int(mouse_y // 200)
                click_col = int(mouse_x // 200)

                if available_square(click_row, click_col):
                    mark_square(click_row, click_col, player)
                    if check_win(player):
                        game_over = True
                        last_screen(player)
                    player = player % 2 + 1
                    draw_figure()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_r:
                    restart()
                    player = 1
                    game_over = False

        pygame.display.update()
        clock.tick(60)
def text():
    font = pygame.font.SysFont('comicsansms', 60)
    font2 = pygame.font.SysFont('comicsansms', 40)
    font3 = pygame.font.SysFont('comicsansms', 20)

    text1 = font.render('Tic Tac Toe', True, white)
    text2 = font2.render('Press Enter To Start Game', True, white)
    text3 = font3.render('If You Want to Restart Game Press R', True, white)
    window.blit(text1,(130,30))
    window.blit(text2,(50,420))
    window.blit(text3,(120,500))
    pygame.display.update()

bg=pygame.image.load('bg2.png')

def last_screen_text(player):
    font = pygame.font.SysFont('comicsansms', 60)
    font2 = pygame.font.SysFont('comicsansms', 40)
    font3 = pygame.font.SysFont('comicsansms', 30)
    if player==1:
        t='Player1 is Win'
    elif player==2:
        t='Player2 is Win'

    text1 = font.render('Congratulation', True, white)
    text2 = font2.render(t, True, white)
    text3 = font3.render('If You Want to Play Again', True, white)
    text4 = font3.render( ' Press R', True, white)
    window.blit(text1,(120,30))
    window.blit(text2,(180,170))
    window.blit(text3,(120,300))
    window.blit(text4,(250,380))
    pygame.display.update()

def welcome():
    exit_status = True
    while exit_status:
        window.fill(welcome_bg_Color)
        window.blit(bg, (170, 140))
        text()

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()

            if event.type==pygame.KEYDOWN:
                if event.key==pygame.K_RETURN:
                    game_loop()

        pygame.display.update()
        clock.tick(60)
welcome()
pygame.quit()
sys.exit()
