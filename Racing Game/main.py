import pygame
import sys
from pygame import mixer
import random
import time

pygame.init()

# =======================Color Variable==========
grey = (65, 66, 84)
color1 = (52, 51, 82)
# =======================Variables=====================
screen_width = 800
screen_height = 600
pause = False
exit_status = False
clock = pygame.time.Clock()
cars = random.choice(['1.png', '2.png', '3.png', '4.png', '5.png', '6.png', '7.png', '8.png', '9.png', '10.png'])
# =======================Screen========================
window = pygame.display.set_mode((screen_width, screen_height))
pygame.display.set_caption('HyerBosster Racing Game')

# ========================Images and icon===================================
icon = pygame.image.load('icon.ico')
pygame.display.set_icon(icon)

carimg = pygame.image.load(cars)
car_width, car_height = carimg.get_rect().size

grassimg_left = pygame.image.load('grass.jpg')
grassimg_left = pygame.transform.scale(grassimg_left, (100, 600)).convert_alpha()

grassimg_right = pygame.transform.rotate(grassimg_left, 180)

roadimg = pygame.image.load('road.jpg')
roadimg = pygame.transform.scale(roadimg, (600, 600)).convert_alpha()

welcomeimg = pygame.image.load('background.png')
welcomeimg = pygame.transform.scale(welcomeimg, (800, 600)).convert_alpha()

introduction_img = pygame.image.load('bg3.jpg')
introduction_img = pygame.transform.scale(introduction_img, (800, 600)).convert_alpha()

pauseimg = pygame.image.load('bg3.jpg')
pauseimg = pygame.transform.scale(pauseimg, (800, 600)).convert_alpha()

channel = pygame.mixer.Channel(0)
channel.set_volume(1)

blast_music = pygame.mixer.Sound('crash.wav')


def car(x, y):
    window.blit(carimg, (x, y))
    pygame.display.update()


def grass_left(x, y):
    window.blit(grassimg_left, (x, y))
    pygame.display.update()


def grass_right(x, y):
    window.blit(grassimg_right, (x, y))
    pygame.display.update()


def road_img(x, y):
    window.blit(roadimg, (x, y))
    pygame.display.update()


def text_objects(text, font):
    textsurface = font.render(text, True, (255, 255, 255))
    return textsurface, textsurface.get_rect()


def message_display(text):
    largetext = pygame.font.Font('freesansbold.ttf', 80)
    textsurf, textrect = text_objects(text, largetext)
    textrect.center = ((screen_width / 2), (screen_height / 2))
    window.blit(textsurf, textrect)
    pygame.display.update()
    time.sleep(3)
    game_loop()


def crash():
    message_display('You Crashed')


def obstacle(obs_startx, obs_starty, obs):
    if obs == 0:
        obs_pic = pygame.image.load('1.png')
        obs_pic = pygame.transform.rotate(obs_pic, 180)
    elif obs == 1:
        obs_pic = pygame.image.load('2.png')
        obs_pic = pygame.transform.rotate(obs_pic, 180)
    elif obs == 2:
        obs_pic = pygame.image.load('3.png')
        obs_pic = pygame.transform.rotate(obs_pic, 180)
    elif obs == 3:
        obs_pic = pygame.image.load('4.png')
        obs_pic = pygame.transform.rotate(obs_pic, 180)
    elif obs == 4:
        obs_pic = pygame.image.load('5.png')
        obs_pic = pygame.transform.rotate(obs_pic, 180)
    elif obs == 5:
        obs_pic = pygame.image.load('6.png')
        obs_pic = pygame.transform.rotate(obs_pic, 180)
    elif obs == 6:
        obs_pic = pygame.image.load('7.png')
        obs_pic = pygame.transform.rotate(obs_pic, 180)
    elif obs == 7:
        obs_pic = pygame.image.load('8.png')
        obs_pic = pygame.transform.rotate(obs_pic, 180)
    elif obs == 8:
        obs_pic = pygame.image.load('9.png')
        obs_pic = pygame.transform.rotate(obs_pic, 180)
    elif obs == 9:
        obs_pic = pygame.image.load('10.png')
        obs_pic = pygame.transform.rotate(obs_pic, 180)

    width, height = obs_pic.get_rect().size
    window.blit(obs_pic, (obs_startx, obs_starty))
    pygame.display.update()
    return width, height


def score_system(passed, score):
    font = pygame.font.SysFont(None, 28)
    text = font.render('Passed:' + str(passed), True, (255, 255, 255))
    score = font.render('Score:' + str(score), True, (255, 255, 255))

    window.blit(score, (5, 50))
    window.blit(text, (5, 80))
    pygame.display.update()


def game_loop():
    global exit_status, pause
    mixer.music.load('sound2.mp3')
    mixer.music.set_volume(0.5)
    mixer.music.play()
    exit_status = False
    x = (screen_width * 0.45)
    y = (screen_height * 0.8)
    x_change = 0

    obstacle_speed = 9
    obs = 0
    y_change = 0
    obs_startx = random.randrange(200, (screen_width - 200))
    obs_starty = -750

    passes = 0
    level = 0
    score = 0
    y2 = 7
    while not exit_status:

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                exit_status = True
                sys.exit()

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    x_change = -5

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_RIGHT:
                    x_change = 5
                if event.key == pygame.K_a:  # accelerate
                    obstacle_speed += 2
                if event.key == pygame.K_b:  # break
                    obstacle_speed -= 2
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_RIGHT or event.key == pygame.K_LEFT:
                    x_change = 0

        x += x_change
        pause = True
        rel_y_left = y2 % grassimg_left.get_rect().width
        window.blit(grassimg_left, (0, rel_y_left - grassimg_left.get_rect().width))

        rel_y_right = y2 % grassimg_right.get_rect().width
        window.blit(grassimg_right, (700, rel_y_right - grassimg_right.get_rect().width))

        rel_y_road = y2 % roadimg.get_rect().width
        window.blit(roadimg, (100, rel_y_road - roadimg.get_rect().width))

        if rel_y_left < 800 or rel_y_road < 800 or rel_y_right < 800:
            window.blit(grassimg_left, (0, rel_y_left))
            window.blit(grassimg_right, (700, rel_y_right))
            window.blit(roadimg, (100, rel_y_road))
        y2 += obstacle_speed

        obs_starty -= obstacle_speed / 4
        obs_width, obs_height = obstacle(obs_startx, obs_starty, obs)
        obs_starty += obstacle_speed
        car(x, y)
        score_system(passes, score)
        if x < 90 or x > screen_width - (car_width + 90):
            exit_status = True

            channel.play(blast_music, maxtime=30)
            crash()
        if obs_starty > screen_height:

            obs_starty = 0 - obs_height
            obs_startx = random.randrange(170, (screen_width - 170))
            obs = random.randrange(0, 9)
            passes += 1
            score = passes * 10
            if int(passes) % 10 == 0:
                level += 1
                obstacle_speed += 2
                largetext = pygame.font.Font('freesansbold.ttf', 80)
                textsurf, textrect = text_objects('LEVEL' + str(level), largetext)
                textrect.center = ((screen_width / 2), (screen_height / 2))
                window.blit(textsurf, textrect)
                pygame.display.update()
                time.sleep(3)
        if y < obs_starty + obs_height:

            if x > obs_startx and x < obs_startx + obs_width or x + car_width > obs_startx and x + car_width < obs_startx + obs_width:
                channel.play(blast_music, maxtime=60)
                crash()
        button('Pause', 700, 2, 100, 50, blue, color1, "pause")

        pygame.display.update()
        clock.tick(100)


def welcome_text():
    font = pygame.font.SysFont(None, 70)
    text = font.render('HyerBosster Racing Game', True, (41, 36, 54))

    window.blit(text, (screen_width / 2 - 300, screen_height / 2 - 200))

    pygame.display.update()


green = (0, 255, 0)
bright_green = (14, 125, 51)
bright_red = (87, 55, 55)
red = (255, 0, 0)
blue = (24, 96, 122)
bright_blue = (0, 0, 256)


def countdown_background():
    font = pygame.font.SysFont(None, 25)
    x = (screen_width * 0.45)
    y = (screen_height * 0.8)
    x_change = 0

    x_left_grass = 0
    y_left_grass = 0

    x_right_grass = 700
    y_right_grass = 0

    grass_left(x_left_grass, y_left_grass)
    grass_right(x_right_grass, y_right_grass)
    road_img(100, 0)
    car(x, y)
    score = font.render('Score: 0', True, (255, 255, 255))
    text = font.render('Dodged: 0', True, (255, 255, 255))

    window.blit(score, (2, 30))
    window.blit(text, (2, 50))
    button('Pause', 700, 2, 100, 50, blue, color1, "pause")

    pygame.display.update()
    clock.tick(100)


def countdown():
    count = True
    while count:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()

        countdown_background()
        largetext = pygame.font.Font('freesansbold.ttf', 115)
        textsurf, textrect = text_objects('3', largetext)
        textrect.center = ((screen_width / 2), (screen_height / 2))
        window.blit(textsurf, textrect)
        pygame.display.update()
        clock.tick(1)

        countdown_background()
        largetext = pygame.font.Font('freesansbold.ttf', 115)
        textsurf, textrect = text_objects('2', largetext)
        textrect.center = ((screen_width / 2), (screen_height / 2))
        window.blit(textsurf, textrect)
        pygame.display.update()
        clock.tick(1)

        countdown_background()
        largetext = pygame.font.Font('freesansbold.ttf', 115)
        textsurf, textrect = text_objects('1', largetext)
        textrect.center = ((screen_width / 2), (screen_height / 2))
        window.blit(textsurf, textrect)
        pygame.display.update()
        clock.tick(1)

        countdown_background()
        largetext = pygame.font.Font('freesansbold.ttf', 115)
        textsurf, textrect = text_objects('GO', largetext)
        textrect.center = ((screen_width / 2), (screen_height / 2))
        window.blit(textsurf, textrect)
        pygame.display.update()
        clock.tick(1)
        game_loop()


def introduction():
    intro = True

    while intro:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                intro = False
                sys.exit()

        window.blit(introduction_img, (0, 0))
        largetext = pygame.font.Font('freesansbold.ttf', 50)
        largetext1 = pygame.font.Font('freesansbold.ttf', 50)
        smalltext = pygame.font.Font('freesansbold.ttf', 25)
        mediumtext = pygame.font.Font('freesansbold.ttf', 20)
        textsurf, textrect = text_objects('Instruction', largetext)
        textrect.center = (400, 40)
        window.blit(textsurf, textrect)
        textsurf1, textrect1 = text_objects('This Is Car Game In Which You Dodge the Coming Cars', smalltext)
        textrect1.center = (400, 100)
        window.blit(textsurf1, textrect1)

        textsurf2, textrect2 = text_objects('Controls', largetext1)
        textrect2.center = (400, 300)
        window.blit(textsurf2, textrect2)

        textsurf3, textrect3 = text_objects('Accelerator : A', mediumtext)
        textrect3.center = (400, 350)
        window.blit(textsurf3, textrect3)

        textsurf3, textrect3 = text_objects('Break : B', mediumtext)
        textrect3.center = (400, 380)
        window.blit(textsurf3, textrect3)

        textsurf3, textrect3 = text_objects('Pause : P', mediumtext)
        textrect3.center = (400, 410)
        window.blit(textsurf3, textrect3)

        textsurf3, textrect3 = text_objects('Left Turn : Left Arrow', mediumtext)
        textrect3.center = (400, 440)
        window.blit(textsurf3, textrect3)

        textsurf3, textrect3 = text_objects('Right Turn : Right Arrow', mediumtext)
        textrect3.center = (400, 470)
        window.blit(textsurf3, textrect3)
        button('Back', 20, 10, 100, 40, blue, color1, 'menu')

        pygame.display.update()
        clock.tick(60)


def paused():
    global pause
    while pause:
        window.blit(pauseimg, (0, 0))
        largetext = pygame.font.Font('freesansbold.ttf', 80)
        textsurf, textrect = text_objects('Paused', largetext)
        textrect.center = ((screen_width / 2), (screen_height / 2))
        window.blit(textsurf, textrect)
        button('Continue', 150, 450, 150, 50, green, bright_green, 'unpause')
        button('Restart', 350, 450, 150, 50, blue, color1, 'play')
        button('Main Menu', 550, 450, 200, 50, red, bright_red, 'menu')
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()

        pygame.display.update()
        clock.tick(60)


def unpause():
    global pause
    pause = False


def button(msg, x, y, w, h, ic, ac, action=None):
    mouse = pygame.mouse.get_pos()

    click = pygame.mouse.get_pressed()

    if x + w > mouse[0] > x and y + h > mouse[1] > y:
        pygame.draw.rect(window, ac, (x, y, w, h))

        if click[0] == 1 and action != None:
            if action == 'play':
                countdown()
            elif action == 'quit':
                pygame.quit()
                quit()
                sys.exit()
            elif action == 'intro':
                introduction()
            elif action == 'menu':
                welcome()
            elif action == 'pause':
                paused()
            elif action == 'unpause':
                unpause()
    else:
        pygame.draw.rect(window, ic, (x, y, w, h))

    smalltext = pygame.font.Font('freesansbold.ttf', 20)
    textsurf, textrect = text_objects(msg, smalltext)
    textrect.center = ((x + (w / 2)), (y + (h / 2)))
    window.blit(textsurf, textrect)
    pygame.display.update()


def welcome():
    global exit_status
    mixer.music.load('front_music.mp3')
    mixer.music.set_volume(0.5)
    mixer.music.play()
    while not exit_status:

        window.blit(welcomeimg, (0, 0))
        welcome_text()
        button('Start', 150, 480, 100, 50, green, bright_green, 'play')
        button('Quit', 550, 480, 100, 50, red, bright_red, 'Quit')
        button('Instruction', 300, 480, 200, 50, blue, color1, 'intro')
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                exit_status = True
                sys.exit()

        pygame.display.update()
        clock.tick(60)


welcome()
pygame.quit()
sys.exit()
